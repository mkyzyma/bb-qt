pragma Singleton
import QtQuick 2.9

QtObject {
    property color floreColor: "lightBlue"
    property color ballColor: "#A0EF6C00"
    property color wallColor: "steelBlue" //Qt.darker(floreColor, 2)
    property color foodColor: "green"
    property color outerColor: "lightgray"
    property color enemyColor: "brown"
}
